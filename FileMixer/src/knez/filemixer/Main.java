package knez.filemixer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unixolika aplikacija koja treba da iz jednog foldera u drugi kopira neki broj
 * slucajno izabranih fajlova. 
 * @author Milos Milutinovic
 */
public class Main {

	// Podesavanja sa default vrednostima.
	public static boolean rekurzivno = false;
	public static int trazeniBroj = 50;
	public static boolean brojJeProcenat = false;
	public static String izvoriste = ".";
	public static String destinacija = "./mix";
	
	// TODO 99: Osmisliti i implementirati parametar koji ce odrediti metod generisanja spiska fajlova
	// TODO 50: Osmisliti i implementirati parametar - dozvoljeni tip fajlova
	/**
	 * @param args <br/>
	 * Ulazni argumenti iz konzole.<br/>
	 * <br/>
	 * FORMAT:<br/>
	 * filemixer -jar &lt;izvoriste&gt; &lt;odrediste&gt; &lt;kolicina&gt; [opcije]<br/>
	 * <br/>
	 * <b>izvoriste</b> - Folder odakle se uzimaju fajlovi. Default - tekuci.<br/>
	 * <b>odrediste</b> - Folder u koji se smestaju fajlovi. Default - tekuci/mix<br/>
	 * <b>kolicina</b> - Koliko fajlova premestiti. Ceo broj ili procenat. Default - 50<br/>
	 * <b>opcije</b> - Dodatne opcije<br/>
	 * <br/>
	 * <b>opcije</b>:<br/>
	 * -R - Program ce (rekurzivno) u izbor fajlova ubaciti i sve fajlove iz potfoldera izvorista<br/>
	 */
	public static void main(String[] args) {
		// NAPOMENA: Trenutno su podrzane samo tri stvari od navedenih:
		// -R za rekurziju i -procenat/-broj za odredjivanje broja fajlova 
		// (sto treba izmeniti), a nije podrzano izvoriste/odrediste sto je po defaultu... default.
		
		// TODO 1: Preuzeti parametre izvoriste odrediste broj koji moraju da budu zadati tim redosledom.
		// Dozvoljene su sledece varijante ovih parametara:
		// - Nema nijednog parametra => sve vrednosti ce biti default
		// - Ima jedan parametar, broj => uzimas ga, a izvoriste i odrediste su default 
		// - Ima jedan parametar, tekst => uzimas ga kao odrediste, a izvoriste i broj su default
		// - Ima dva parametra, tekst i broj => uzimas ih kao odrediste i broj, izvoriste je default
		// - Ima dva parametra, tekst i tekst => uzimas ih kao izvoriste i odrediste, broj je default
		// - Ima sva tri parametra => znas sta ti je ciniti; sve ih uzimas
		
		// TODO 2: Kada uzmes parametar broj, ako se zavrsava sa znakom % onda je procenat,
		// u suprotnom je obican broj. Koristis promenljivu brojJeProcenat da razgranicis,
		// a dobijeni broj u svakom slucaju belezis u trazeniBroj promenljivu
		
		// TODO 3: sta preostane od parametara kada proveris tri "obavezna", tj. izvoriste,
		// odrediste i broj, su opcije i mogu da idu bilo kojim redosledom. Napisati kod
		// koji dzara po tim preostalim parametrima i proverava koje opcije su ukljucene
		// (a to je trenutno samo -R).
		
		// bolje da parametri budu u listi nego u nizu, lakse za manipulaciju
		List<String> parametri = Arrays.asList(args);
		
		// Cupamo parametre bez nekog reda. Ako ima nesto, ima.
		if(parametri.contains("-R")) {
			rekurzivno = true;
		}
		if(parametri.contains("-procenat")) {
			int gde = parametri.indexOf("-procenat");
			brojJeProcenat = true;
			trazeniBroj = Integer.parseInt(parametri.get(gde+1));// ovo zbog formata "-procenat xx"
		}
		if(parametri.contains("-broj")) {
			int gde = parametri.indexOf("-broj");
			brojJeProcenat = false;
			trazeniBroj = Integer.parseInt(parametri.get(gde+1));// ovo zbog formata "-broj xx"
		}
		
		// Generise spisak fajlova za kopiranje
		List<File> fajlovi = generisiSpisakFajlova(izvoriste);
		
		// Kreira destinacioni folder ako ne postoji
		File destinacijaFolder = new File(destinacija);
		destinacijaFolder.mkdirs();
		
		// Kopira sve fajlove sa spiska u njega
		for(File rezultujuci : fajlovi) {
			kopirajJedanFajl(rezultujuci);
		}
	}
	
	/*
	// OVO JE ZAKOMENTARISANO SA RAZLOGOM. NE DIRATI ZA SADA.
	private static void ucitajKonfFajl(String odakle) {
		Properties konfPodesavanja = new Properties();
		InputStream fis;
		try {
			fis = new FileInputStream("podesavanja.ini");
			konfPodesavanja.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void ucitajSkriptu(String odakle) {}
	*/
	
	/**
	 * Generise spisak fajlova koje treba kopirati, koristeci zadate parametre.
	 * @param putanje	jedna ili vise zadatih putanja odakle ce se kopirati random fajlovi.
	 */
	private static List<File> generisiSpisakFajlova(String ... putanje) {
		// NAPOMENA: SA RAZLOGOM JE String ... putanje. OVO NE DIRATI ZA SADA.
		
		//TODO 4: Ovde se generise spisak fajlova koje treba kopirati, a ako je
		// stavljeno da se ti fajlovi kupe iz TEKUCEG foldera, e onda mozda imas problem.
		// Sta je zapravo tekuci folder? Tamo gde se nalazis u konzoli kada pokrenes
		// program, ili tamo gde se program nalazi? Utvrditi.
		
		//TODO 5: Ako se fajlovi za kopiranje uzimaju iz istog foldera gde se nalazi ovaj
		// program, onda treba ugraditi kod da program ignorise sam svoj fajl
		// (imgmixer.jar ili kako god)
		
		List<File> fajlovi = new ArrayList<File>();
		for(String putanja : putanje) {
			File fajl = new File(putanja);
			fajlovi.addAll(sviFajlovi(fajl, rekurzivno));
		}
		
		int brojFajlova = fajlovi.size();
		if(brojJeProcenat) {
			trazeniBroj = (trazeniBroj * brojFajlova)/100;
			brojJeProcenat = false;
		}
		trazeniBroj = trazeniBroj > brojFajlova? brojFajlova : trazeniBroj;
		
		Collections.shuffle(fajlovi);
		List<File> rezultat = fajlovi.subList(0, trazeniBroj);
		
		return rezultat;
	}
	
	/**
	 * Generise spisak svih fajlova u jednom folderu (i njegovim potfolderima,
	 * ako je rekurzivno true).
	 * @param folder - folder ciji se fajlovi dodaju na spisak
	 * @param rekurzivno - da li se dodaje i sadrzaj potfoldera?
	 * @return spisak svih fajlova u zadatom folderu
	 */
	private static List<File> sviFajlovi(File folder, boolean rekurzivno) {
		List<File> fajlovi = new ArrayList<File>();		
		File[] lista = folder.listFiles();
		if(lista != null) {
			for(File tempFajl : lista) {
				if(tempFajl.isDirectory() && rekurzivno) {
					fajlovi.addAll(sviFajlovi(tempFajl, rekurzivno));
				} else {
					if(!tempFajl.isDirectory()) fajlovi.add(tempFajl);
				}
			}
		}
		return fajlovi;
	}
	
	/**
	 * Kopira jedan fajl.
	 * @param iz - fajl koji se kopira. Kopira ga u default destinaciju (bezveze)
	 */
	private static void kopirajJedanFajl(File iz) {
		//TODO 6: Promeniti kod da ne koristi globalnu promenjlivu destinacija
		// vec da to prima kao parametar, pa da potpis funkcije bude kopirajJedanFajl(iz,u)
		
		//TODO 7: Srediti try/catch tako da se uzme u obzir to da ako pukne kod negde
		// moras da proveris sta si otvorio od streamova do tada (from, to, ili obojicu)
		// i onda da probas da ih zatvoris (sto opet moze da baca izuzetak)
		try {
			FileInputStream from = new FileInputStream(iz);
	        FileOutputStream to = new FileOutputStream(destinacija + File.separator + iz.getName());
	        byte[] buffer = new byte[4096];
	        int bytesRead;
	
	        while ((bytesRead = from.read(buffer)) != -1) {
	            to.write(buffer, 0, bytesRead);
	        }
	        from.close();
	        to.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}

